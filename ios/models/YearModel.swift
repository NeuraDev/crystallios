//
//  YearModel.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class YearModel {
    let title: String
    let count: Int
    let year: Int
    let month: Int
    
    init(title: String, count: Int, year: Int, month: Int) {
        self.title = title
        self.count = count
        self.year = year
        self.month = month
    }
}
