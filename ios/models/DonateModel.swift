//
//  DonateModel.swift
//  ios
//
//  Created by Гладков Алексей on 15.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import StoreKit

class DonateModel {
    let id: String
    let title: String
    let subtitle: String
    let price: String
    let product: SKProduct
    
    init(id: String, title: String, subtitle: String, price: String, product: SKProduct) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.price = price
        self.product = product
    }
}
