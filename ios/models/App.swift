//
//  App.swift
//  ios
//
//  Created by Алексей Гладков on 17.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class App {
    var sounds = true
    var vibration = true
    var notifications = true
    
    static var shared = App()
}
