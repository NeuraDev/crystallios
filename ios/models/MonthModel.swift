//
//  MonthModel.swift
//  ios
//
//  Created by Гладков Алексей on 27.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class MonthModel {
    var dayNumber: Int
    var records: Int
    var crystallCount: Int
    
    init(dayNumber: Int, crystallCount: Int, records: Int) {
        self.dayNumber = dayNumber
        self.crystallCount = crystallCount
        self.records = records
    }
}
