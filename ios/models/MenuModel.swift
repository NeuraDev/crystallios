//
//  MenuModel.swift
//  ios
//
//  Created by Алексей Гладков on 12.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
class MenuModel {
    let icon: String
    let title: String
    
    init(icon: String, title: String) {
        self.icon = icon
        self.title = title
    }
}
