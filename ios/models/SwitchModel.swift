//
//  SwitchModel.swift
//  ios
//
//  Created by Гладков Алексей on 16.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class SwitchModel {
    let id: Int
    let title: String
    let isOn: Bool
    
    init(id: Int, title: String, isOn: Bool) {
        self.id = id
        self.title = title
        self.isOn = isOn
    }
}
