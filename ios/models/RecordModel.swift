//
//  RecordModel.swift
//  ios
//
//  Created by Гладков Алексей on 22.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class RecordModel {
    let title: String
    let dateTime: String

    init(title: String, dateTime: String) {
        self.title = title
        self.dateTime = dateTime
    }
}

