//
//  UIImageView.swift
//  ios
//
//  Created by Гладков Алексей on 22.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func animateWithGIF(name: String){
        let htmlString: String =    "<!DOCTYPE html><html><head><title></title></head>" +
            "<body style=\"background-color: transparent;\">" +
            "<img src=\""+name+"\" align=\"middle\" style=\"width:100%;height:100%;\">" +
            "</body>" +
        "</html>"
        
        let path: NSString = Bundle.main.bundlePath as NSString
        let baseURL: URL = URL(fileURLWithPath: path as String) // to load images just specifying its name without full path
        
        let frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        let gifView = UIWebView(frame: frame)
        
        gifView.isOpaque = false // The drawing system composites the view normally with other content.
        gifView.backgroundColor = UIColor.clear
        gifView.loadHTMLString(htmlString, baseURL: baseURL)
        
        var s: [UIView] = self.subviews
        for i in 0 ..< s.count {
            if s[i].isKind(of: UIWebView.self) { s[i].removeFromSuperview() }
        }
        
        self.addSubview(gifView)
    }
    
    func animateWithGIF(url: String){
        self.animateWithGIF(name: url)
    }
}
