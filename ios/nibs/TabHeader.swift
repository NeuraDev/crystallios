//
//  TabHeader.swift
//  ios
//
//  Created by Гладков Алексей on 13.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class TabHeader: UIView {
    @IBOutlet weak var txtTitle: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    static func instantiateFromNib() -> TabHeader {
        let views = Bundle.main.loadNibNamed("TabHeader", owner: nil, options: nil)
        return views?.first as! TabHeader
    }
}
