//
//  DayCell.swift
//  ios
//
//  Created by Гладков Алексей on 26.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import Toast_Swift
import PopupDialog

class DayCell: UITableViewCell {
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    @IBOutlet weak var viewSurface: UIView!
    @IBOutlet weak var txtContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        viewSurface.layer.cornerRadius = 8
        viewSurface.layer.shadowColor = UIColor.black.cgColor
        viewSurface.layer.shadowOpacity = 0.12
        viewSurface.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewSurface.layer.shadowRadius = 2
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(copyText(_:)))
        self.addGestureRecognizer(gestureRecognizer)
    }

    @objc func copyText(_ sender: UILongPressGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.began) {
            let menuVC = DayMenu(nibName: nil, bundle: nil)
            let dialog = PopupDialog(viewController: menuVC, preferredWidth: 280, gestureDismissal: true)
            menuVC.popup = dialog
            menuVC.text = txtContent.text
            menuVC.container = container
            
            let overlayAppearance = PopupDialogOverlayView.appearance()
            let containerAppearance = PopupDialogContainerView.appearance()
            
            overlayAppearance.blurEnabled = false
            overlayAppearance.color = UIColor.clear
            containerAppearance.cornerRadius = 16
            
            container?.present(dialog, animated: true, completion: nil)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private var container: UIViewController? = nil
    func setupCell(model: RecordModel, container: UIViewController) {
        txtContent.text = model.title
        self.container = container
    }
}
