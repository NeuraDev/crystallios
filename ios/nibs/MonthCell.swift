//
//  MonthCell.swift
//  ios
//
//  Created by Гладков Алексей on 13.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class MonthCell: UITableViewCell {
    @IBOutlet weak var txtPercent: UILabel!
    @IBOutlet weak var txtRecords: UILabel!
    @IBOutlet weak var txtMonth: UILabel!
    @IBOutlet weak var txtDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
