//
//  SettingsCell.swift
//  ios
//
//  Created by Гладков Алексей on 16.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    @IBOutlet weak var marginStartBottom: NSLayoutConstraint!
    @IBOutlet weak var dividerBottom: UIView!
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var txtTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
