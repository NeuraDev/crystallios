//
//  DonateCell.swift
//  ios
//
//  Created by Гладков Алексей on 26.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class DonateCell: UITableViewCell {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
