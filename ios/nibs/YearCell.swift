//
//  YearCell.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class YearCell: UITableViewCell {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
