//
//  MenuCell.swift
//  ios
//
//  Created by Гладков Алексей on 24.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var marginStartBottom: NSLayoutConstraint!
    @IBOutlet weak var dividerBottom: UIView!
    @IBOutlet weak var dividerTop: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
