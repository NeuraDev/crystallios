//
//  CrystalRepository.swift
//  ios
//
//  Created by Гладков Алексей on 23.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CrystalRepository {
    let context: NSManagedObjectContext
    let entityName = "CrystalEntity"
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func insert(model: CrystallModel) {
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let managedObject = NSManagedObject(entity: entityDescription!, insertInto: context)
        
        managedObject.setValue(model.dateTime, forKey: "dateTime")
        do {
            try context.save()
        } catch {
            print("Error inserting record \(error.localizedDescription)")
        }
    }
    
    func fetchCrystals(forDay: String, _ callback:@escaping (Bool, Int) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                var count = 0
                var hasDate = false
                let results = try self.context.fetch(fetchRequest)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                let dateFormatter2 = DateFormatter()
                dateFormatter2.dateFormat = "dd.MM.yyyy"
                
                if (results.count > 0) {
                    DispatchQueue.main.async {
                        results.forEach({ (result) in
                            let dateTime = (result as? NSManagedObject)?.value(forKey: "dateTime") as? String ?? "14.06.1989 00:00:00"
                            let converted = dateFormatter2.string(from: dateFormatter.date(from: dateTime)!)
                            
                            if (converted == forDay) {
                                count = count + 1
                                hasDate = true
                            }
                        })
                        
                        callback(hasDate, count)
                        
                    }
                } else {
                    DispatchQueue.main.async {
                        callback(false, 0)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    callback(false, 0)
                }
            }
        }
    }
    
    func fetchToday(_ callback:@escaping (Int) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                let today = Calendar.current.component(.day, from: Date())
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                var count = 0
                DispatchQueue.main.async {
                    results.forEach({ (result) in
                        if let obj = result as? NSManagedObject {
                            let date = obj.value(forKey: "dateTime") as! String
                            let crystalDay = Calendar.current.component(.day, from: dateFormatter.date(from: date)!)
                            if today == crystalDay {
                                count = count + 1
                            }
                        }
                    })
                    
                    callback(count)
                }
            } catch {
                DispatchQueue.main.async {
                    callback(0)
                }
            }
        }
    }
    
    func fetchAllCrystals(_ callback:@escaping (Int) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                
                DispatchQueue.main.async {
                    callback(results.count)
                }
            } catch {
                DispatchQueue.main.async {
                    callback(0)
                }
            }
        }
    }
}
