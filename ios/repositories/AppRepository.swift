//
//  AppRepository.swift
//  ios
//
//  Created by Алексей Гладков on 17.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class AppRepository {
    let context: NSManagedObjectContext
    let entityName = "AppEntity"
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func loadConfiguration() {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            
            do {
                let results = try
                    self.context.fetch(fetchRequest)
                if results.count > 0 {
                    let result = results[0]
                    App.shared.notifications = (result as! NSManagedObject).value(forKey: "notifications") as! Bool
                    
                    App.shared.sounds = (result as! NSManagedObject).value(forKey: "sounds") as! Bool
                    
                    App.shared.vibration = (result as! NSManagedObject).value(forKey: "vibration") as! Bool
                } else {
                    let entityDescription = NSEntityDescription.entity(forEntityName: self.entityName, in: self.context)
                    let managedObject = NSManagedObject(entity: entityDescription!, insertInto: self.context)
                    
                    managedObject.setValue(true, forKey: "notifications")
                    managedObject.setValue(true, forKey: "sounds")
                    managedObject.setValue(true, forKey: "vibration")
                    
                    do {
                        try self.context.save()
                    } catch {
                        print("Error inserting record \(error.localizedDescription)")
                    }
                }
            } catch {
                print("Error fetching records \(error.localizedDescription)")
            }
        }
    }
    
    func updateConfiguration() {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            
            do {
                let results = try
                    self.context.fetch(fetchRequest)
                if results.count > 0 {
                    let result = results[0]
                    (result as! NSManagedObject).setValue(App.shared.notifications, forKey: "notifications")
                    
                    (result as! NSManagedObject).setValue(App.shared.sounds, forKey: "sounds")
                    
                    (result as! NSManagedObject).setValue(App.shared.vibration, forKey: "vibration")
                    
                    do {
                        try self.context.save()
                    } catch {
                        print("Error inserting record \(error.localizedDescription)")
                    }
                } else {
                    let entityDescription = NSEntityDescription.entity(forEntityName: self.entityName, in: self.context)
                    let managedObject = NSManagedObject(entity: entityDescription!, insertInto: self.context)
                    
                    managedObject.setValue(true, forKey: "notifications")
                    managedObject.setValue(true, forKey: "sounds")
                    managedObject.setValue(true, forKey: "vibration")
                    
                    do {
                        try self.context.save()
                    } catch {
                        print("Error inserting record \(error.localizedDescription)")
                    }
                }
            } catch {
                print("Error fetching records \(error.localizedDescription)")
            }
        }
    }
}
