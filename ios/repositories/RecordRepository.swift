//
//  RecordRepository.swift
//  ios
//
//  Created by Гладков Алексей on 23.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class RecordRepository {
    let context: NSManagedObjectContext
    let entityName = "RecordEntity"
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func insert(model: RecordModel) {
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let managedObject = NSManagedObject(entity: entityDescription!, insertInto: context)
        
        managedObject.setValue(0, forKey: "id")
        managedObject.setValue(model.title, forKey: "title")
        managedObject.setValue(model.dateTime, forKey: "datetime")
        do {
            try context.save()
        } catch {
            print("Error inserting record \(error.localizedDescription)")
        }
    }
    
    func fetch(by Day: Int, and Month: Int, and Year: Int, _ callback:@escaping ([RecordModel]) -> Void) {
        fetch { (success, data) in
            if let data = data {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                var sorted = [RecordModel]()
                data.forEach({ (model) in
                    let year = Calendar.current.component(.year, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let month = Calendar.current.component(.month, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let day = Calendar.current.component(.day, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    
                    if (day == Day && month == Month && year == Year) {
                        sorted.append(model)
                    }
                })
                callback(sorted)
            }
        }
    }
    
    func fetchRecords(by Month: Int, and Year: Int, _ callback:@escaping ([Int: [RecordModel]]) -> Void) {
        fetch { (success, data) in
            if let data = data {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                var sorted = [Int: [RecordModel]]()
                data.forEach({ (model) in
                    let year = Calendar.current.component(.year, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let month = Calendar.current.component(.month, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let day = Calendar.current.component(.day, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    
                    if (month == Month && year == Year) {
                        if (sorted[day] == nil) {
                            sorted[day] = [RecordModel]()
                        }
                        
                        sorted[day]?.append(model)
                    }
                })
                
                callback(sorted)
            }
        }
    }
    
    func fetch(by Month: Int, and Year: Int, _ callback:@escaping ([MonthModel]) -> Void) {
        fetch { (success, data) in
            if let data = data {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                var sorted = [MonthModel]()
                data.forEach({ (model) in
                    let year = Calendar.current.component(.year, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let month = Calendar.current.component(.month, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let day = Calendar.current.component(.day, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    
                    if (year == Year && month == Month) {
                        if let index = sorted.index(where: { $0.dayNumber == day }) {
                            sorted[index].records = sorted[index].records + 1
                        } else {
                            let model = MonthModel(dayNumber: day, crystallCount: 0, records: 1)
                            sorted.append(model)
                        }
                    }
                })
                
                callback(sorted)
            }
        }
    }
    
    func fetchYear(_ callback:@escaping ([Int: [Int: [String]]]?) -> Void) {
        fetch { (success, data) in
            if let data = data {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                
                var sorted = [Int: [Int: [String]]]()
                
                data.forEach({ (model) in
                    let year = Calendar.current.component(.year, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    let month = Calendar.current.component(.month, from: dateFormatter.date(from: model.dateTime) ?? Date())
                    
                    if sorted[year] != nil {
                        if sorted[year]![month] != nil {
                            print("sorted month true")
                            sorted[year]![month]!.append(model.title)
                        } else {
                            sorted[year]![month] = [String]()
                            sorted[year]![month]!.append(model.title)
                        }
                    } else {
                        sorted[year] = [Int: [String]]()
                        sorted[year]![month] = [String]()
                        sorted[year]![month]!.append(model.title)
                    }
                })
                
                print("sorted \(sorted)")
                callback(sorted)
            } else {
                callback(nil)
            }
        }
    }
    
    func fetch(_ callback:@escaping (Bool, [RecordModel]?) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                
                var records = [RecordModel]()
                for result in results.reversed() {
                    let recordModel = RecordModel(title: String(describing: (result as! NSManagedObject).value(forKey: "title")!),
                                                  dateTime: String(describing: (result as! NSManagedObject).value(forKey: "datetime")!))
                    records.append(recordModel)
                }
                
                DispatchQueue.main.async {
                    callback(true, records)
                }
            } catch {
                DispatchQueue.main.async {
                    callback(false, nil)
                }
                print("Error fetching records \(error.localizedDescription)")
            }
        }
    }
}
