//
//  TemplateRepository.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class TemplateRepository {
    let context: NSManagedObjectContext
    let entityName = "TemplateEntity"
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func insert(newValue: String) {
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let managedObject = NSManagedObject(entity: entityDescription!, insertInto: context)
        
        managedObject.setValue(newValue, forKey: "text")
    
        do {
            try context.save()
        } catch {
            print("Error inserting template \(error.localizedDescription)")
        }
    }
    
    func update(newValue: String, index: Int, _ callback:@escaping (Bool) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                
                if results.count > index {
                    let result = results.reversed()[index] as! NSManagedObject
                    result.setValue(newValue, forKey: "text")
                    
                    do {
                        try self.context.save()
                        DispatchQueue.main.async {
                            callback(true)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            callback(false)
                        }
                        print("Error inserting template \(error.localizedDescription)")
                    }
                } else {
                    DispatchQueue.main.async {
                        callback(true)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    callback(false)
                }
                
                print("Error fetching records \(error.localizedDescription)")
            }
        }
    }
    
    func delete(index: Int, _ callback:@escaping (Bool) -> Void) {
        DispatchQueue.global().async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                
                if results.count > index {
                    self.context.delete(results.reversed()[index] as! NSManagedObject)
                }
                
                DispatchQueue.main.async {
                    callback(true)
                }
            } catch {
                DispatchQueue.main.async {
                    callback(false)
                }
                
                print("Error fetching records \(error.localizedDescription)")
            }
        }
    }
    
    func fetch(_ callback:@escaping ([String]?) -> Void) {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
            do {
                let results = try self.context.fetch(fetchRequest)
                var templates = [String]()
            
                for result in results.reversed() {
                    if let managedObject = result as? NSManagedObject {
                        if let value = (managedObject.value(forKey: "text") as? String) {
                            templates.append(value)
                        }
                    }
                }
            
                callback(templates)
            } catch {
                callback(nil)
                print("Error fetching records \(error.localizedDescription)")
            }
    }
}
