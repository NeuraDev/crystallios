//
//  DayMenuView.swift
//  ios
//
//  Created by Гладков Алексей on 01.03.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class DayMenuView: UIView {

    // The title label
    lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    // The table view
    lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        // Add views
        addSubview(titleLabel)
        addSubview(tableView)
        
        // Setup constraints
        heightAnchor.constraint(equalToConstant: 148).isActive = true
        
        var constraints = [NSLayoutConstraint]()
        let views: [String: UIView] = ["titleLabel": titleLabel, "tableView": tableView]
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|[titleLabel(50)][tableView]|", options: [], metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|[titleLabel]|", options: [], metrics: nil, views: views)
        NSLayoutConstraint.activate(constraints)
    }
}
