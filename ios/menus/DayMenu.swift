//
//  DayMenu.swift
//  ios
//
//  Created by Гладков Алексей on 01.03.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PopupDialog
import Toast_Swift

class DayMenu: UIViewController {
    fileprivate let displayData = [NSLocalizedString("menu_copy", comment: ""),
                                   NSLocalizedString("menu_template", comment: "")]
    fileprivate let cellReuseIdentifier = String(describing: PopupCell.self)
    fileprivate let tableView = UITableView()
    fileprivate let menuView = DayMenuView()
    fileprivate let repository = TemplateRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    public weak var popup: PopupDialog?
    public weak var container: UIViewController?
    public var text: String?
    
    override func loadView() {
        self.view = menuView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.titleLabel.text = NSLocalizedString("menu_title", comment: "")
        
        menuView.tableView.delegate = self
        menuView.tableView.dataSource = self
        menuView.tableView.register(UINib(nibName: "PopupNib", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        menuView.tableView.isScrollEnabled = false
        menuView.tableView.tableFooterView = UIView(frame: CGRect.zero)

        let header = UIView(frame: CGRect(x: 0, y: 0, width: menuView.tableView.frame.width, height: 0.5))
        header.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        menuView.tableView.tableHeaderView = header
        
        // Do any additional setup after loading the view.
//        self.view.addSubview(UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 150)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DayMenu: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PopupCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! PopupCell
        cell.txtTitle.text = displayData[indexPath.row]
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        switch indexPath.row {
        case 0:
            container?.view.makeToast(NSLocalizedString("copy_text", comment: ""), duration: 1.0, position: .bottom)
            UIPasteboard.general.string = text
            break
            
        case 1:
            container?.view.makeToast(NSLocalizedString("template_text", comment: ""), duration: 1.0, position: .bottom)
            if let strongText = text {
                repository.insert(newValue: strongText)
            }
            break
        default:
            break
        }
        
        popup?.dismiss()
    }
}
