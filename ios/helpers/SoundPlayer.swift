//
//  SoundPlayer.swift
//  ios
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import AVFoundation

enum Ext: String {
    case MP3 = "mp3"
    case WAV = "wav"
}

class SoundPlayer {
    var player: AVAudioPlayer? = nil
    
    func playSound(name: String, ext: Ext) {
        print("1")
        guard let url = Bundle.main.url(forResource: name, withExtension: ext.rawValue) else { return }
        print("2 \(url)")
        do {
            print("3")
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            print("4")
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            print("5")
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            print("6")
            guard let player = player else { return }
            print("7")
            player.play()
            print("8")
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
