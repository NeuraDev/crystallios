//
//  Converters.swift
//  ios
//
//  Created by Гладков Алексей on 26.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class Converters {
    
    static func getMonth(byId: Int) -> String {
        switch byId {
        case 1:
            return NSLocalizedString("month_jan", comment: "")
        case 2:
            return NSLocalizedString("month_feb", comment: "")
        case 3:
            return NSLocalizedString("month_mar", comment: "")
        case 4:
            return NSLocalizedString("month_apr", comment: "")
        case 5:
            return NSLocalizedString("month_may", comment: "")
        case 6:
            return NSLocalizedString("month_jun", comment: "")
        case 7:
            return NSLocalizedString("month_jul", comment: "")
        case 8:
            return NSLocalizedString("month_aug", comment: "")
        case 9:
            return NSLocalizedString("month_sep", comment: "")
        case 10:
            return NSLocalizedString("month_oct", comment: "")
        case 11:
            return NSLocalizedString("month_nov", comment: "")
        default:
            return NSLocalizedString("month_dec", comment: "")
        }
    }
}
