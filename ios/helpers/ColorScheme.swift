//
//  ColorScheme.swift
//  ios
//
//  Created by Алексей Гладков on 12.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

class ColorScheme {
    static let navBar = 0x602F9F
    static let appTint = 0x00F4FE
    static let linkColor = 0x04C6CE
    static let bgColor = 0xF9F9F9
    static let separatorColor = 0xc8c7cc
}
