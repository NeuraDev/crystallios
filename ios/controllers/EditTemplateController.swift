//
//  EditTemplateController.swift
//  ios
//
//  Created by Алексей Гладков on 17.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class EditTemplateController: UIViewController {
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var txtNew: UITextView!
    var index = 0
    var text = ""
    
    fileprivate let repository = TemplateRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)

    override func viewDidLoad() {
        super.viewDidLoad()
        if (text == "") {
            self.navigationController?.popViewController(animated: true)
        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save(_:)))
        
        // Do any additional setup after loading the view.
        txtNew.text = text
        txtNew.textContainerInset = UIEdgeInsetsMake(16, 16, 16, 16)
    }

    @IBAction func deleteTemplate(_ sender: Any) {
        let alertController = UIAlertController(title: NSLocalizedString("delete_title", comment: ""),
                                                message:NSLocalizedString("delete_content", comment: ""),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("delete_ok", comment: ""),
                                                style: .default, handler: { (action) in
                                                    self.repository.delete(index: self.index, { [weak self] (true) in
                                                        self?.navigationController?.viewControllers.forEach({ (vc) in
                                                            if (vc is AddController) {
                                                                self?.navigationController?.popToViewController(vc, animated: true)
                                                            }
                                                        })
                                                    })
        }))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("delete_cancel", comment: ""),
                                                style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func save(_ sender: Any?) {
        repository.update(newValue: txtNew.text, index: index) { (success) in
            self.navigationController?.viewControllers.forEach({ (vc) in
                if (vc is AddController) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension EditTemplateController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("template_add_hint", comment: "")
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        height.constant = textView.contentSize.height
        self.view.layoutIfNeeded()
    }
}
