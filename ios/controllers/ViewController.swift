//
//  ViewController.swift
//  ios
//
//  Created by Гладков Алексей on 22.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import CoreData
import PopupDialog
import AudioToolbox.AudioServices
import SwiftyGif
import SwiftySound

class ViewController: UIViewController {

    fileprivate var stone: Stone? = nil
    fileprivate var currentStone: UIImageView? = nil
    fileprivate var currentPosition = Position.top
    fileprivate let txtCounter = UILabel()
    fileprivate let repository = CrystalRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)

    fileprivate var imageView = UIImageView()
    fileprivate var txtHelp = UILabel()
    
    enum Position: Int {
        case top = 0, bottom = 1
    }
    
    @objc func clearCrystal(_ sender: Any?) {
        self.txtCounter.text = "0"
        self.txtCounter.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.imageView.removeFromSuperview()
            self.imageView = UIImageView(gifImage: UIImage(gifName: "grand-crystall-gif"), loopCount: -1)
            
            self.imageView.stopAnimatingGif()
            self.imageView.frame = CGRect(x: (self.view.frame.width / 2) - 98, y: 40, width: 196, height: 196)
            self.imageView.isUserInteractionEnabled = true
            self.imageView.delegate = self
            self.view.addSubview(self.imageView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addRecord(_:)))
        gestureRecognizer.numberOfTapsRequired = 1
        
        self.txtHelp.frame = CGRect(x: self.view.frame.width / 2 - 35,
                                    y: self.view.frame.height / 2 - 35, width: 70, height: 70)
        txtHelp.text = ""
        txtHelp.font = UIFont.systemFont(ofSize: 70)
        self.view.addSubview(txtHelp)
        
        let date = Date().tomorrow.startOfDay
//        let date = Date().addingTimeInterval(5)
        let timer = Timer(fireAt: date, interval: 0, target: self, selector: #selector(clearCrystal(_:)), userInfo: nil, repeats: false)
        RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
        
        repository.fetchCrystals(forDay: Date().toString(pattern: "dd.MM.yyyy")) { [weak self] (success, count) in
            guard let strongSelf = self else { return }
//            strongSelf.title = "Главная"
            strongSelf.view.isUserInteractionEnabled = true
            strongSelf.stone = Stone(container: strongSelf.view)
            strongSelf.stone?.stoneCount = count
            
            if (count >= 0 && count < 7) {
                strongSelf.imageView = UIImageView(gifImage: UIImage(gifName: "grand-crystall-gif"), loopCount: -1)
            } else if (count >= 7 && count < 14) {
                strongSelf.imageView = UIImageView(gifImage: UIImage(gifName: "grand-crystall-gif2"), loopCount: -1)
            } else if (count >= 14 && count < 21) {
                strongSelf.imageView = UIImageView(gifImage: UIImage(gifName: "grand-crystall-gif3"), loopCount: -1)
            } else {
                strongSelf.imageView = UIImageView(gifImage: UIImage(gifName: "grand-crystall-gif4"), loopCount: -1)
            }
        
            strongSelf.imageView.stopAnimatingGif()
            strongSelf.imageView.frame = CGRect(x: (strongSelf.view.frame.width / 2) - 98, y: 40, width: 196, height: 196)
            strongSelf.imageView.isUserInteractionEnabled = true
            strongSelf.imageView.addGestureRecognizer(gestureRecognizer)
            strongSelf.imageView.delegate = self
            strongSelf.view.addSubview(strongSelf.imageView)
            
            strongSelf.txtCounter.frame = CGRect(x: strongSelf.view.frame.width / 2 - 128, y: 72, width: 256, height: 128)
            strongSelf.txtCounter.text = "\(count)"
            strongSelf.txtCounter.textColor = UIColor.white
            strongSelf.txtCounter.alpha = 1
            strongSelf.txtCounter.font = UIFont.systemFont(ofSize: 32, weight: .medium)
            strongSelf.txtCounter.textAlignment = .center
            strongSelf.txtCounter.isHidden = count == 0
            
            strongSelf.txtCounter.layer.shadowColor = UIColor(rgb: 0x271338).cgColor
            strongSelf.txtCounter.layer.shadowOpacity = 1
            strongSelf.txtCounter.layer.shadowOffset = CGSize(width: 0, height: 1)
            strongSelf.txtCounter.layer.shadowRadius = 0
            
            strongSelf.view.addSubview(strongSelf.txtCounter)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        repository.fetchCrystals(forDay: Date().toString(pattern: "dd.MM.yyyy")) { [weak self] (success, count) in
            guard let strongSelf = self else { return }
            strongSelf.txtCounter.text = "\(count)"
            strongSelf.txtCounter.isHidden = count == 0
                
            if (count >= 0 && count < 7) {
                strongSelf.imageView.gifImage = UIImage(gifName: "grand-crystall-gif")
            } else if (count >= 7 && count < 14) {
                strongSelf.imageView.gifImage = UIImage(gifName: "grand-crystall-gif2")
            } else if (count >= 14 && count < 21) {
                strongSelf.imageView.gifImage = UIImage(gifName: "grand-crystall-gif3")
            } else {
                strongSelf.imageView.gifImage = UIImage(gifName: "grand-crystall-gif4")
            }
        }
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self.view)
            let leftEdge = self.view.frame.width / 2 - 64
            let rightEdge = self.view.frame.width / 2 + 64
            let topEdge = self.view.frame.height - 128
            let bottomEdge = self.view.frame.height
            
            if (location.x >= leftEdge && location.x <= rightEdge && location.y >= topEdge && location.y <= bottomEdge) {
                let touchPosition = location.y > (self.view.frame.height / 2) ? Position.bottom : Position.top
                currentStone = stone?.generate(startX: Int(location.x), startY: Int(location.y), position: touchPosition.rawValue)
                currentPosition = Position.bottom
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let strongCurrent = currentStone else { return }
        guard let strongStone = stone else { return }
        if let touch = touches.first {
            let location = touch.location(in: self.view)
            let border: CGFloat = self.view.frame.height - 190
            let touchPosition = location.y > border ? Position.bottom : Position.top
            strongStone.move(stone: strongCurrent, newX: location.x, newY: location.y)
            
            if (touchPosition.rawValue != currentPosition.rawValue) {
                currentPosition = touchPosition
                crossMiddle(state: touchPosition.rawValue)
            }
        }
    }
    
    // OLD NewX: self.currentPosition == Position.top ? self.view.frame.width / 2 : location.x
    // OLD NewY: self.currentPosition == Position.top ? 48 : (self.view.frame.height - CGFloat(Stone.stoneHeight))
    // OLD TextCounter: strongStone.stoneCount > Stone.maxStoneCount ? ">100%" : "\(strongStone.stoneCount)"
    // OLD isDec: self.currentPosition == Position.bottom
    
    // NO BORDER NewX: self.view.frame.width / 2
    // NO BORDER NewY: 48
    // NO BORDER isDec: false
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let strongCurrent = currentStone else { return }
        guard let strongStone = stone else { return }
        if let touch = touches.first {
            let location = touch.location(in: self.view)
            UIView.animate(withDuration: 1, animations: {
                strongStone.move(stone: strongCurrent,
                                 newX: self.currentPosition == Position.top ? self.view.frame.width / 2 : location.x,
                                 newY: self.currentPosition == Position.top ? 48 : (self.view.frame.height - CGFloat(Stone.stoneHeight)))
            })
        
            UIView.animate(withDuration: 1, animations: {
                strongStone.disappear(stone: strongCurrent, isDec: self.currentPosition == Position.bottom)
                self.txtCounter.text = "\(strongStone.stoneCount)"
            })
            
            onRelease(state: currentPosition.rawValue)
            currentStone = nil
        }
    }
    
    @IBAction func showRecord(_ sender: Any) {
        self.performSegue(withIdentifier: "RecordSegue", sender: self)
    }
    
    @IBAction func addRecord(_ sender: Any) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Addition", bundle: nil)
        let addController = storyBoard.instantiateViewController(withIdentifier: "Add")
        self.navigationController?.pushViewController(addController, animated: true)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch cancelled")
    }
}

extension ViewController: SwiftyGifDelegate {
    func gifDidLoop(sender: UIImageView) {
        imageView.stopAnimatingGif()
    }
}

extension ViewController: MapInterface {
    func onHold() {
        
    }
    
    func onRelease(state: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        switch state {
        case Position.top.rawValue:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.imageView.startAnimatingGif()
            }
            
            stone?.highlight(stone: currentStone, container: self.view)
            
            if (App.shared.vibration) {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
            
            if (App.shared.sounds) {
                Sound.play(file: "bells.mp3")
            }
            
            repository.insert(model: CrystallModel(dateTime: dateFormatter.string(from: Date())))
            
            let count = stone?.stoneCount ?? 0
            if (count >= 0 && count < 7) {
                imageView.gifImage = UIImage(gifName: "grand-crystall-gif")
            } else if (count >= 7 && count < 14) {
                imageView.gifImage = UIImage(gifName: "grand-crystall-gif2")
            } else if (count >= 14 && count < 21) {
                imageView.gifImage = UIImage(gifName: "grand-crystall-gif3")
            } else {
                imageView.gifImage = UIImage(gifName: "grand-crystall-gif4")
            }
            
            if (count == 4) {
                txtHelp.text = "👍"
                bounceHelp()
            } else if (count == 11) {
                txtHelp.text = "😁"
                bounceHelp()
            } else if (count == 17) {
                txtHelp.text = "🙏"
                bounceHelp()
            }
            
            txtCounter.isHidden = false
            let stoneCount = stone?.stoneCount ?? 0
            if (stoneCount == 21) {
                Sound.play(file: "fanfar.mp3")
                let dialog = PopupDialog(title: NSLocalizedString("21_title", comment: ""),
                                         message: NSLocalizedString("21_message", comment: ""),
                                         image: UIImage(named: "ic_grats"))
                
                let vc = dialog.viewController as! PopupDialogDefaultViewController
                vc.titleColor = UIColor(rgb: UInt(ColorScheme.navBar))
                vc.titleFont = UIFont.boldSystemFont(ofSize: 26)
            
                let overlayAppearance = PopupDialogOverlayView.appearance()
                let containerAppearance = PopupDialogContainerView.appearance()
                
                overlayAppearance.blurEnabled = false
                overlayAppearance.color = UIColor.clear
                containerAppearance.cornerRadius = 16
                self.present(dialog, animated: true, completion: nil)
            }
            
            if (stoneCount == 25 || stoneCount == 50 || stoneCount == 75 || stoneCount == 100) {
                let descriptionDialog = PopupDialog(title: NSLocalizedString("more_about", comment: ""), message: AboutHelper.english)
                let overlayAppearance = PopupDialogOverlayView.appearance()
                
                overlayAppearance.blurEnabled = false
                overlayAppearance.color = UIColor.clear
                self.present(descriptionDialog, animated: true, completion: nil)
            }
            
            break

        case Position.bottom.rawValue:
            break

        default:
            break
        }
    }
    
    func crossMiddle(state: Int) {
        switch state {
        case Position.top.rawValue:
            stone?.highlight(stone: currentStone, container: self.view)
            break
            
        case Position.bottom.rawValue:
//            stone?.shutdown(stone: currentStone)
            break
            
        default:
            break
        }
    }
    
    private func bounceHelp() {
        self.txtHelp.frame = CGRect(x: self.view.frame.width / 2 - 35,
                                    y: self.view.frame.height / 2 - 35, width: 70, height: 70)
        self.txtHelp.alpha = 1
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping:0.4, initialSpringVelocity:0, options: .curveEaseInOut, animations: {
            self.txtHelp.frame = CGRect(x: self.view.frame.width / 2 - 45,
                                        y: self.view.frame.height / 2 - 45,
                                        width: 90, height: 90)
        }, completion: { (success) in
            UIView.animate(withDuration: 0.5, animations: {
                self.txtHelp.alpha = 0
            })
        })
    }
}
