//
//  DonateController.swift
//  ios
//
//  Created by Гладков Алексей on 26.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit

class DonateController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate let cellIdentifier = String(describing: DonateCell.self)
    fileprivate var dataToDisplay = [DonateModel]()
    fileprivate var skuProducts = [SKProduct]()
    @IBOutlet weak var txtNoDonates: UILabel!
    @IBOutlet weak var loaderSend: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        dataToDisplay.append(DonateModel(id: 0, title: "Water", subtitle: "Пригласите нас на чашечку кофе!", price: "0,5$"))
//        dataToDisplay.append(DonateModel(id: 0, title: "Fresh", subtitle: "Пришлите нам корзинку свежих фруктов!", price: "2$"))
//        dataToDisplay.append(DonateModel(id: 0, title: "Lunch", subtitle: "Угостите нас обедом!", price: "10$"))
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 32))
        headerView.backgroundColor = UIColor.clear
        let dividerView = UIView(frame: CGRect(x: 0, y: 31.5, width: self.view.frame.width, height: 0.5))
        dividerView.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        headerView.addSubview(dividerView)
        
        loaderSend.hidesWhenStopped = true
        loaderSend.startAnimating()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DonateNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.tableHeaderView = headerView
        // Do any additional setup after loading the view
        txtNoDonates.text = NSLocalizedString("donates_no_items", comment: "")
        tableView.isHidden = true
        txtNoDonates.isHidden = true
        
        SwiftyStoreKit.retrieveProductsInfo([Products.coffee, Products.fresh, Products.lunch]) { [weak self] result in
            self?.loaderSend.stopAnimating()
            if (result.retrievedProducts.count == 0) {
                self?.tableView.isHidden = true
                self?.txtNoDonates.isHidden = false
            } else {
                result.retrievedProducts.forEach({ [weak self] (product) in
                    print("product \(product.productIdentifier)")
                    self?.dataToDisplay.append(DonateModel(id: product.productIdentifier,
                                                           title: product.localizedTitle,
                                                           subtitle: product.localizedDescription,
                                                           price: product.localizedPrice!,
                                                           product: product))
                })
                
                self?.txtNoDonates.isHidden = true
                self?.tableView.isHidden = false
                self?.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
}

extension DonateController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DonateCell
        if (dataToDisplay[indexPath.row].title == "") {
            switch (dataToDisplay[indexPath.row].id) {
            case "crystall.coffee":
                cell.txtTitle.text = NSLocalizedString("donate_coffee", comment: "")
                break
                
            case "crystall.fresh":
                cell.txtTitle.text = NSLocalizedString("donate_fresh", comment: "")
                break
                
            case "crystall.lunch":
                cell.txtTitle.text = NSLocalizedString("donate_lunch", comment: "")
                break
                
            default:
                break
            }
        } else {
            cell.txtTitle.text = dataToDisplay[indexPath.row].title
        }
        cell.txtPrice.text = dataToDisplay[indexPath.row].price
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        SwiftyStoreKit.purchaseProduct(dataToDisplay[indexPath.row].product) { (purchase) in
            print("products purchased")
        }
    }
}
