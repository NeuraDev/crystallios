//
//  RecordController.swift
//  ios
//
//  Created by Гладков Алексей on 22.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PDFGenerator
import PopupDialog
import VGParallaxHeader

class RecordController: UIViewController {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    fileprivate let headerView = TabHeader.instantiateFromNib()
    
    let sectionHeaderHeight: CGFloat = 53.5
    var data = [Int: [Int: [String]]]()
    var sections = [Int]()
    var sectionsCount = [Int]()
    fileprivate let cellIdentifier = String(describing: YearCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = NSLocalizedString("title_journal", comment: "")

        txtTitle.text = NSLocalizedString("title_journal", comment: "")
        txtTitle.alpha = 0
        headerView.txtTitle.text = NSLocalizedString("title_journal", comment: "")
        tableView.setParallaxHeader(headerView, mode: .fill, height: 116)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: "YearNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 48.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        let repository = RecordRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
        repository.fetchYear { [weak self] (models) in
            guard let strongSelf = self else { return }
            if let strongData = models {
                strongSelf.sectionsCount.removeAll()
                strongSelf.sections.removeAll()
                
                strongData.forEach({ (key, value) in
                    strongSelf.sections.append(key)
                    var count = 0
                    strongData[key]?.forEach({ (key1, value1) in
                        count = count + value1.count
                    })
                    
                    strongSelf.sectionsCount.append(count)
                })
                
                strongSelf.data.removeAll()
                strongSelf.data = strongData
                strongSelf.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RecordController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tableView.shouldPositionParallaxHeader()
        let progress = scrollView.parallaxHeader.progress
        if (progress < 0.7) {
            UIView.animate(withDuration: 0.1, animations: {
                self.txtTitle.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: {
                self.txtTitle.alpha = 0
            })
        }
        
        let size = 34 * scrollView.parallaxHeader.progress
        headerView.txtTitle.font = UIFont.systemFont(ofSize: size > 48 ? 48 : size < 34 ? 34 : size)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let recordData = data[sections[section]] {
            return recordData.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: sectionHeaderHeight))
        view.backgroundColor = UIColor.clear
        let label = UILabel(frame: CGRect(x: 16, y: 24, width: Int(tableView.bounds.width - 32), height: 22))
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.black
        
        label.text = "\(String(describing: sections[section]))"
        
        let separator = UIView(frame: CGRect(x: 0, y: 53, width: tableView.bounds.width, height: 0.5))
        separator.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        view.addSubview(label)
        view.addSubview(separator)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! YearCell
        if let recordData = data[sections[indexPath.section]] {
            cell.txtTitle.text = Converters.getMonth(byId: Array(recordData.keys)[indexPath.row])
            cell.txtCount.text = "\(String(describing: recordData[Array(recordData.keys)[indexPath.row]]!.count))"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard:UIStoryboard = UIStoryboard(name: "Journal", bundle: nil)
        let monthController = storyBoard.instantiateViewController(withIdentifier: "Month") as! MonthController
        if let recordData = data[sections[indexPath.section]] {
            monthController.monthNumber = Array(recordData.keys)[indexPath.row]
            monthController.yearNumber = Array(data.keys)[indexPath.section]
        }
        self.navigationController?.pushViewController(monthController, animated: true)
    }
}
