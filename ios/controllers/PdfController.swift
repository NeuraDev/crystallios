//
//  PdfController.swift
//  ios
//
//  Created by Гладков Алексей on 05.03.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PDFGenerator

class PdfController: UIViewController {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnExport: UIButton!
    @IBOutlet weak var tableView: UITableView!
    fileprivate let cellIdentifier = String(describing: DayCell.self)
    var isDay = true
    var dayData = [RecordModel]()
    var monthNumber = 0
    var monthAllData = [Int: [RecordModel]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DayNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = .none
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0

        // Do any additional setup after loading the view.
        txtTitle.text = NSLocalizedString("pdf_title", comment: "")
        btnExport.setTitle(NSLocalizedString("pdf_export", comment: ""), for: .normal)
        btnClose.setTitle(NSLocalizedString("pdf_close", comment: ""), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func export(_ sender: Any) {
        let dst = URL(fileURLWithPath: NSTemporaryDirectory().appending("crystalResult.pdf"))
        
        // writes to Disk directly.
        do {
            try PDFGenerator.generate([tableView], to: dst)
            let objectsToShare = [dst]
            let shareVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
            self.present(shareVC, animated: true, completion: nil)
        } catch (let error) {
            print(error)
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PdfController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if (isDay) {
            return 1
        } else {
            return monthAllData.keys.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (isDay) {
            return 0
        } else {
            return 36
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (isDay) {
            return UIView(frame: CGRect.zero)
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 16, width: tableView.frame.width, height: 20))
            view.backgroundColor = UIColor.clear
            
            let txtTitle = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 36))
            txtTitle.text = "\(Array(monthAllData.keys)[section]) \(Converters.getMonth(byId: monthNumber))"
            txtTitle.textColor = UIColor(rgb: 0x2F2F2F)
            txtTitle.font = UIFont.boldSystemFont(ofSize: 16)
            txtTitle.textAlignment = .center
            view.addSubview(txtTitle)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isDay) {
            return dayData.count
        } else {
            if let value = monthAllData[Array(monthAllData.keys)[section]] {
                return value.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DayCell
        cell.selectionStyle = .none
//        cell.topMargin.constant = 0
        
        if (isDay) {
            cell.setupCell(model: dayData[indexPath.row], container: self)
        } else {
            if let value = monthAllData[Array(monthAllData.keys)[indexPath.section]] {
                cell.setupCell(model: value[indexPath.row], container: self)
            }
        }
        
        return cell
    }
}
