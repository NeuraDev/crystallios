//
//  DayController.swift
//  ios
//
//  Created by Гладков Алексей on 26.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PDFKit
import PDFGenerator

class DayController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate var displayData = [RecordModel]()
    fileprivate let cellIdentifier = String(describing: DayCell.self)
    fileprivate let repository = RecordRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)

    var monthNumber = 1
    var dayNumber = 1
    var yearNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DayNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = .none
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        
        self.title = "\(dayNumber) \(Converters.getMonth(byId: monthNumber))"
        
        let buttonPDF: UIButton = UIButton()
        buttonPDF.setImage(UIImage(named: "icon-navbar-export"), for: .normal)
        buttonPDF.addTarget(self, action: #selector(printPdf(_:)), for: .touchUpInside)
        buttonPDF.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let buttonShare: UIButton = UIButton()
        buttonShare.setImage(UIImage(named: "icon-navbar-share"), for: .normal)
        buttonShare.addTarget(self, action: #selector(share(_:)), for: .touchUpInside)
        buttonShare.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barButtonPDF = UIBarButtonItem(customView: buttonPDF)
        let barButtonShare = UIBarButtonItem(customView: buttonShare)
        self.navigationItem.rightBarButtonItems = [barButtonPDF]
        
        // Do any additional setup after loading the view.
        repository.fetch(by: dayNumber, and: monthNumber, and: yearNumber) { [weak self] (data) in
            self?.displayData.removeAll()
            self?.displayData.append(contentsOf: data)
            self?.tableView.reloadData()
        }
    }
    
    @objc func printPdf(_ sender: Any?) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Journal", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "Pdf") as! PdfController
        controller.isDay = true
        controller.dayData = displayData
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func share(_ sender: Any?) {
        let shareText = NSLocalizedString("commend_text", comment: "")
        let shareImage: UIImage = UIImage(named: "1024")!
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [shareImage, shareText], applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DayController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DayCell
        cell.selectionStyle = .none
        cell.setupCell(model: displayData[indexPath.row], container: self)
        cell.transform = CGAffineTransform(scaleX: 1, y: -1)

//        if (indexPath.row == 0) {
//            cell.topMargin.constant = 16
//        }
        return cell
    }
}
