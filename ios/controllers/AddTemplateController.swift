//
//  AddTemplateController.swift
//  ios
//
//  Created by Алексей Гладков on 17.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class AddTemplateController: UIViewController {
    @IBOutlet weak var txtNew: UITextView!
    @IBOutlet weak var height: NSLayoutConstraint!
    
     fileprivate let repository = TemplateRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save(_:)))

        // Do any additional setup after loading the view.
        txtNew.textContainerInset = UIEdgeInsetsMake(16, 16, 16, 16)
        txtNew.text = NSLocalizedString("template_add_hint", comment: "")
        txtNew.textColor = UIColor.lightGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func save(_ sender: Any?) {
        repository.insert(newValue: txtNew.text)
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddTemplateController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("template_add_hint", comment: "")
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
        
    func textViewDidChange(_ textView: UITextView) {
        height.constant = textView.contentSize.height
        self.view.layoutIfNeeded()
    }
}
