//
//  MonthController.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class MonthController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate var data = [MonthModel]()
    fileprivate let cellIdentifier = String(describing: MonthCell.self)
    fileprivate let repository = RecordRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    fileprivate let crystall = CrystalRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    var monthNumber = 1
    var yearNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        headerView.backgroundColor = UIColor.white
        headerView.isUserInteractionEnabled = true
        
        let headerTitle = UILabel(frame: CGRect(x: 16, y: 16, width: headerView.frame.width - 32, height: 50 - 32))
        headerTitle.text = NSLocalizedString("month_header", comment: "")
        headerTitle.textColor = UIColor(rgb: UInt(ColorScheme.linkColor))
        headerTitle.font = UIFont(name: "SFProDisplay-regular", size: 15)
        headerView.addSubview(headerTitle)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showAll(sender:)))
        headerView.addGestureRecognizer(tapGesture)
        
        let separatorView = UIView(frame: CGRect(x: 0, y: 49.5, width: headerView.frame.width, height: 0.5))
        separatorView.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        headerView.addSubview(separatorView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MonthNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.backgroundColor = UIColor(rgb: UInt(ColorScheme.bgColor))
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.title = "\(Converters.getMonth(byId: monthNumber))"
        let month = monthNumber < 10 ? "0\(monthNumber)" : "\(monthNumber)"
        
        
        // Do any additional setup after loading the view.
        repository.fetch(by: monthNumber, and: yearNumber) { [weak self] (fetchedData) in
            self?.data = fetchedData
            self?.tableView.reloadData()
            
            self?.data.forEach({ (model) in
                let day = model.dayNumber < 10 ? "0\(model.dayNumber)" : "\(model.dayNumber)"
                let dateSearch = "\(day).\(month).\(String(describing: self?.yearNumber ?? 1989))"
                self?.crystall.fetchCrystals(forDay: dateSearch, { [weak self] (success, count) in
                    if let index = self?.data.index(where: { $0.dayNumber == model.dayNumber }) {
                        self?.data[index].crystallCount = count
                        self?.tableView.reloadData()
                    }
                })
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func showAll(sender: Any?) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Journal", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "MonthAll") as! MonthAllController
        controller.monthNumber = monthNumber
        controller.yearNumber = yearNumber
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension MonthController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MonthCell
        cell.txtDay.text = "\(data[indexPath.row].dayNumber)"
        cell.txtMonth.text = "\(Converters.getMonth(byId: monthNumber))"
        cell.txtRecords.text = "\(String(describing: data[indexPath.row].records))"

        let value = (Float(data[indexPath.row].crystallCount) / 21.0) * 100
//        let stringValue = value > 100 ? "> 100" : String(format: "%.01f", value)
        let stringValue = String(format: "%.01f", value)
        cell.txtPercent.text = "\(stringValue)%"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard:UIStoryboard = UIStoryboard(name: "Journal", bundle: nil)
        let dayController = storyBoard.instantiateViewController(withIdentifier: "Day") as! DayController
        dayController.dayNumber = data[indexPath.row].dayNumber
        dayController.monthNumber = monthNumber
        dayController.yearNumber = yearNumber
        self.navigationController?.pushViewController(dayController, animated: true)
    }
}
