//
//  EditDeleteController.swift
//  ios
//
//  Created by Гладков Алексей on 13.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class EditDeleteController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let repository = TemplateRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    fileprivate var dataToDisplay = [String]()
    fileprivate let cellIdentifier = String(describing: TemplateCell.self)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("template_footer", comment: "")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TemplateNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.repository.fetch { [weak self] (data) in
            guard let strongSelf = self else { return }
            if let data = data {
                strongSelf.dataToDisplay.removeAll()
                strongSelf.dataToDisplay.append(contentsOf: data)
                strongSelf.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension EditDeleteController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TemplateCell
        cell.txtTitle.text = dataToDisplay[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Addition", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "EditTemplate") as! EditTemplateController
        controller.text = dataToDisplay[indexPath.row]
        controller.index = indexPath.row
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
