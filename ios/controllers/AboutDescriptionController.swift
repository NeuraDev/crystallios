//
//  AboutDescriptionController.swift
//  ios
//
//  Created by Гладков Алексей on 15.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class AboutDescriptionController: UIViewController {
    @IBOutlet weak var txtDescription: UILabel!
    
    var text = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view..
        txtDescription.setLineSpacing(lineSpacing: 0, lineHeightMultiple: 4)
        txtDescription.text = text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
