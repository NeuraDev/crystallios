//
//  AddController.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PopupDialog

class AddController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    var templateText: String = ""
    
    let sectionHeaderHeight = 24
    fileprivate var count = 0
    fileprivate var displayData = [RecordModel]()
    fileprivate var sectionsCount = 0
    fileprivate let cellIdentifier = String(describing: DayCell.self)
    fileprivate var repository = RecordRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    fileprivate var crystalRepository = CrystalRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UINib(nibName: "DayNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 48.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: .UIKeyboardWillHide, object: nil)
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeKeyboard)))
        crystalRepository.fetchToday { [weak self] (count) in
            self?.count = count
        }

        // Do any additional setup after loading the view.
        let date = Date()
        let day = Calendar.current.component(.day, from: date)
        let month = Calendar.current.component(.month, from: date)
        let year = Calendar.current.component(.year, from: date)
        
        repository.fetch(by: day, and: month, and: year) { [weak self] (data) in
            self?.displayData.removeAll()
            self?.displayData.append(contentsOf: data)
            self?.tableView.reloadData()
        }
    }
    
    @objc func keyboardShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomMargin.constant = -keyboardHeight + (self.tabBarController?.tabBar.frame.height ?? 0)
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardHide() {
        bottomMargin.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func removeKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        if (templateText != "") {
            txtComment.text = templateText
            templateText = ""
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func send(_ sender: Any) {
        if let text = txtComment.text {
            if (text == "") {
                let popupDialog = PopupDialog(title: NSLocalizedString("popup_no_text_title", comment: ""),
                                              message: NSLocalizedString("popup_no_text_message", comment: ""))
                self.present(popupDialog, animated: true, completion: nil)
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
                let recordModel = RecordModel(title: text, dateTime: dateFormatter.string(from: Date()))
                if (displayData.count >= count) {
                    let popupDialog = PopupDialog(title: NSLocalizedString("error_title", comment: ""),
                                                  message: NSLocalizedString("error_many_crystals", comment: ""))
                    self.present(popupDialog, animated: true, completion: nil)
                } else {
                    repository.insert(model: recordModel)
                    displayData.insert(recordModel, at: 0)
                    tableView.reloadData()
                    txtComment.text = ""
                }
            }
        }
    }
    
    @IBAction func showTemplate(_ sender: Any) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Addition", bundle: nil)
        let addController = storyBoard.instantiateViewController(withIdentifier: "Template")
        self.navigationController?.pushViewController(addController, animated: true)
    }
}

extension AddController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DayCell
        cell.setupCell(model: displayData[indexPath.row], container: self)
        cell.transform = CGAffineTransform(scaleX: 1, y: -1)
        if (indexPath.row == 0) {
            cell.topMargin.constant = 16
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
