//
//  MoreController.swift
//  ios
//
//  Created by Гладков Алексей on 24.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class MoreController: UIViewController {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var navBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    fileprivate let cellIdentifier = String(describing: MenuCell.self)
    fileprivate let sectionHeaderHeight: CGFloat = 64
    fileprivate var dataToDisplay = [0 : [MenuModel(icon: "icon-more-notific", title: NSLocalizedString("more_settings", comment: ""))], 1 :
        [MenuModel(icon: "icon-more-about", title: NSLocalizedString("more_about", comment: "")),
         MenuModel(icon: "icon-more-like", title: NSLocalizedString("more_commend", comment: "")),
        MenuModel(icon: "icon-more-gift", title: NSLocalizedString("more_donate", comment: "")),
        MenuModel(icon: "icon-more-rateApp", title: NSLocalizedString("more_rate", comment: ""))]]
    fileprivate let headerView = TabHeader.instantiateFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = NSLocalizedString("title_more", comment: "")
        self.view.backgroundColor = UIColor(rgb: UInt(ColorScheme.bgColor))
        
        txtTitle.alpha = 0
        txtTitle.text = NSLocalizedString("title_settings", comment: "")
        headerView.txtTitle.text = NSLocalizedString("title_settings", comment: "")
        tableView.setParallaxHeader(headerView, mode: .fill, height: 116)
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MenuNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
//        tableView.isScrollEnabled = false
        
        let footerView = UIView(frame: CGRect.zero)
        footerView.backgroundColor = UIColor(rgb: UInt(ColorScheme.bgColor))
        tableView.tableFooterView = footerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MoreController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tableView.shouldPositionParallaxHeader()
        let progress = scrollView.parallaxHeader.progress
        if (progress < 0.7) {
            UIView.animate(withDuration: 0.1, animations: {
                self.txtTitle.alpha = 1
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: {
                self.txtTitle.alpha = 0
            })
        }
        
        let size = 34 * scrollView.parallaxHeader.progress
        headerView.txtTitle.font = UIFont.systemFont(ofSize: size > 48 ? 48 : size < 34 ? 34 : size)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = UIView(frame: CGRect(x: 0, y: 0,
                                                     width: self.view.frame.width,
                                                     height: sectionHeaderHeight))

        let title = UILabel(frame: CGRect(x: 16, y: 32, width: self.view.frame.width - 32, height: 24))
        title.text = section == 0 ? NSLocalizedString("more_header_1", comment: "") : NSLocalizedString("more_header_2", comment: "")
        title.textColor = UIColor.black
        title.font = UIFont.systemFont(ofSize: 14)
        title.backgroundColor = UIColor.clear
        sectionHeaderView.addSubview(title)
        return sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MenuCell
        cell.txtTitle.text = dataToDisplay[indexPath.section]![indexPath.row].title
        cell.imgIcon.image = UIImage(named: dataToDisplay[indexPath.section]![indexPath.row].icon)
        cell.dividerTop.isHidden = indexPath.row != 0
        if (indexPath.section == 0) {
            cell.marginStartBottom.constant = 0
        } else {
            cell.marginStartBottom.constant = indexPath.row == (dataToDisplay[indexPath.section]!.count - 1) ? 0 : 16
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath.section == 1) {
            switch indexPath.row {
            case 0:
                let storyBoard:UIStoryboard = UIStoryboard(name: "More", bundle: nil)
                let controller = storyBoard.instantiateViewController(withIdentifier: "About") as! AboutController
                self.navigationController?.pushViewController(controller, animated: true)
                break
                
            case 2:
                let storyBoard:UIStoryboard = UIStoryboard(name: "More", bundle: nil)
                let donateController = storyBoard.instantiateViewController(withIdentifier: "Donate") as! DonateController
                self.navigationController?.pushViewController(donateController, animated: true)
                break
                
            case 1:
                    let shareText = NSLocalizedString("commend_text", comment: "")
                    let shareImage: UIImage = UIImage(named: "1024")!
                    let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [shareImage, shareText], applicationActivities: nil)
                    self.present(shareVC, animated: true, completion: nil)
                break
                
            case 3:
                let urlStr = "https://itunes.apple.com/us/app/easy-be-happy-21/id1350140560?l=ru&ls=1&mt=8"
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(URL(string: urlStr)!)
                }
                break
                
            default:
                break
            }
        } else {
            let storyBoard: UIStoryboard = UIStoryboard(name: "More", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "Settings") as! SettingsController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
