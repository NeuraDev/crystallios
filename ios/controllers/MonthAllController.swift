//
//  MonthAllController.swift
//  ios
//
//  Created by Гладков Алексей on 15.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit
import PDFGenerator

class MonthAllController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate var data = [Int: [RecordModel]]()
    fileprivate let cellIdentifier = String(describing: DayCell.self)
    fileprivate let repository = RecordRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    var monthNumber = 0
    var yearNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (yearNumber == 0) {
            self.navigationController?.popViewController(animated: true)
        }
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "DayNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        
        self.title = "\(Converters.getMonth(byId: monthNumber))"
        
        let buttonPDF: UIButton = UIButton()
        buttonPDF.setImage(UIImage(named: "icon-navbar-export"), for: .normal)
        buttonPDF.addTarget(self, action: #selector(printPdf(_:)), for: .touchUpInside)
        buttonPDF.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let buttonShare: UIButton = UIButton()
        buttonShare.setImage(UIImage(named: "icon-navbar-share"), for: .normal)
        buttonShare.addTarget(self, action: #selector(share(_:)), for: .touchUpInside)
        buttonShare.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barButtonPDF = UIBarButtonItem(customView: buttonPDF)
        let barButtonShare = UIBarButtonItem(customView: buttonShare)
        self.navigationItem.rightBarButtonItems = [barButtonPDF]
        
        // Do any additional setup after loading the view.
        repository.fetchRecords(by: monthNumber, and: yearNumber) { [weak self] (fetchedData) in
            self?.data = fetchedData
            self?.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func printPdf(_ sender: Any?) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Journal", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "Pdf") as! PdfController
        controller.isDay = false
        controller.monthAllData = data
        controller.monthNumber = monthNumber
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func share(_ sender: Any?) {
        let shareText = NSLocalizedString("commend_text", comment: "")
        let shareImage: UIImage = UIImage(named: "1024")!
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [shareImage, shareText], applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
    }
}

extension MonthAllController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.keys.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 16, width: tableView.frame.width, height: 20))
        view.backgroundColor = UIColor.clear
        
        let txtTitle = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 36))
        txtTitle.text = "\(Array(data.keys)[section]) \(Converters.getMonth(byId: monthNumber))"
        txtTitle.textColor = UIColor(rgb: 0x2F2F2F)
        txtTitle.font = UIFont.boldSystemFont(ofSize: 16)
        txtTitle.textAlignment = .center
        view.addSubview(txtTitle)
        view.transform = CGAffineTransform(scaleX: 1, y: -1)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let value = data[Array(data.keys)[section]] {
            return value.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DayCell
        if let value = data[Array(data.keys)[indexPath.section]] {
            cell.selectionStyle = .none
            cell.setupCell(model: value[indexPath.row], container: self)
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            if (indexPath.row == 0) {
                cell.topMargin.constant = 16
            }
        }
        
        return cell
    }
}
