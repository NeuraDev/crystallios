//
//  SettingsController.swift
//  ios
//
//  Created by Гладков Алексей on 16.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate var displayData = [SwitchModel]()
    fileprivate let cellReuseIdentifier = String(describing: YearCell.self)
    fileprivate let repository = AppRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayData.append(SwitchModel(id: 0, title: NSLocalizedString("settings_notification", comment: ""), isOn: App.shared.notifications))
        displayData.append(SwitchModel(id: 1, title: NSLocalizedString("settings_sounds", comment: ""), isOn: App.shared.sounds))
        displayData.append(SwitchModel(id: 2, title: NSLocalizedString("settings_vibration", comment: ""), isOn: App.shared.vibration))
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SettingsNib", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.backgroundColor = UIColor(rgb: UInt(ColorScheme.bgColor))
        tableView.separatorStyle = .none
//        tableView.separatorInset = UIEdgeInsets.zero
        tableView.allowsSelection = false
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 80 ))
        footerView.backgroundColor = UIColor.clear
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 32))
        headerView.backgroundColor = UIColor.clear
        let dividerView = UIView(frame: CGRect(x: 0, y: 31.5, width: self.view.frame.width, height: 0.5))
        dividerView.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        headerView.addSubview(dividerView)
        
        let footerTitle = UILabel(frame: CGRect(x: 16, y: 16, width: footerView.frame.width - 32, height: 80 - 32))
        footerTitle.text = "The Green Finance Initiative site launched in November 2016 and has helped propel the initiative to the forefront of the UKs strategy on renewable energy and climate change"
        footerTitle.numberOfLines = 0
        footerTitle.textColor = UIColor(rgb: 0x979797)
        footerTitle.font = UIFont.systemFont(ofSize: 12)
        footerTitle.textAlignment = .center
        footerView.addSubview(footerTitle)
        
        tableView.tableFooterView = footerView
        tableView.tableHeaderView = headerView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func switchValue(sender: Any) {
        switch (sender as! UISwitch).tag {
        case 0:
            App.shared.notifications = (sender as! UISwitch).isOn
            repository.updateConfiguration()
            break
            
        case 1:
            App.shared.sounds = (sender as! UISwitch).isOn
            repository.updateConfiguration()
            break
            
        case 2:
            App.shared.vibration = (sender as! UISwitch).isOn
            repository.updateConfiguration()
            break

        default:
            break
        }
    }
}

extension SettingsController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SettingsCell
        cell.txtTitle.text = displayData[indexPath.row].title
        cell.switcher.isOn = displayData[indexPath.row].isOn
        cell.switcher.tag = indexPath.row
        cell.switcher.addTarget(self, action: #selector(switchValue(sender:)), for: .valueChanged)
        cell.marginStartBottom.constant = indexPath.row == (displayData.count - 1) ? 0 : 16
//        cell.separatorInset = indexPath.row == (displayData.count - 1) ? UIEdgeInsets.zero : UIEdgeInsetsMake(0, 16, 0, 0)
        return cell
    }
}
