//
//  AboutController.swift
//  ios
//
//  Created by Гладков Алексей on 15.02.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class AboutController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    fileprivate let dataToDisplay = ["English", "Español", "Deutsch", "Русский", "Italiano", "中国",
                                     "Français", "Portugues"]
    
//    , "हिन्दी" lang in future
    
    let cellReuseIdentifier = String(describing: SimpleCell.self)
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SimpleNib", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorInset = UIEdgeInsets.zero
        
        // Do any additional setup after loading the view.
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 32))
        headerView.backgroundColor = UIColor.clear
        let dividerView = UIView(frame: CGRect(x: 0, y: 31.5, width: self.view.frame.width, height: 0.5))
        dividerView.backgroundColor = UIColor(rgb: UInt(ColorScheme.separatorColor))
        headerView.addSubview(dividerView)
        tableView.tableHeaderView = headerView
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AboutController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SimpleCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SimpleCell
        cell.txtTitle.text = dataToDisplay[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard:UIStoryboard = UIStoryboard(name: "More", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "AboutDescription") as! AboutDescriptionController
        switch (indexPath.row) {
        case 0:
            controller.text = AboutHelper.english
            break
            
        case 1:
            controller.text = AboutHelper.spanish
            break
            
        case 2:
            controller.text = AboutHelper.deutsch
            break
            
        case 3:
            controller.text = AboutHelper.russian
            break
            
        case 4:
            controller.text = AboutHelper.italian
            break
            
        case 5:
            controller.text = AboutHelper.chinese
            break
            
            
        case 6:
            controller.text = AboutHelper.french
            break
            
        case 7:
            controller.text = AboutHelper.portugese
            break
            
        default:
            break
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
