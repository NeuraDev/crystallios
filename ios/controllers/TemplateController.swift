//
//  TemplateController.swift
//  ios
//
//  Created by Гладков Алексей on 25.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import UIKit

class TemplateController: UIViewController {
    @IBOutlet weak var txtNoItems: UILabel!
    @IBOutlet weak var table: UITableView!
    
    fileprivate let repository = TemplateRepository(context: (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext)
    fileprivate var dataToDisplay = [String]()
    fileprivate let cellIdentifier = String(describing: TemplateCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: "TemplateNib", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        table.tableFooterView = UIView(frame: CGRect.zero)
        
        txtNoItems.text = NSLocalizedString("template_no_items", comment: "")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(add(_:)))
        self.title = NSLocalizedString("title_template", comment: "")
        self.repository.fetch { [weak self] (data) in
            guard let strongSelf = self else { return }
            if let data = data {
                strongSelf.dataToDisplay.removeAll()
                strongSelf.dataToDisplay.append(contentsOf: data)
                
                if (strongSelf.dataToDisplay.count > 0) {
                    strongSelf.dataToDisplay.append(NSLocalizedString("template_footer", comment: ""))
                    strongSelf.table.reloadData()
                } else {
                    strongSelf.table.isHidden = true
                    strongSelf.txtNoItems.isHidden = false
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.repository.fetch { [weak self] (data) in
            guard let strongSelf = self else { return }
            if let data = data {
                strongSelf.dataToDisplay.removeAll()
                strongSelf.dataToDisplay.append(contentsOf: data)
                
                if (strongSelf.dataToDisplay.count > 0) {
                    strongSelf.dataToDisplay.append(NSLocalizedString("template_footer", comment: ""))
                    strongSelf.table.reloadData()
                    strongSelf.table.isHidden = false
                    strongSelf.txtNoItems.isHidden = true
                } else {
                    strongSelf.table.isHidden = true
                    strongSelf.txtNoItems.isHidden = false
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func add(_ sender: Any?) {
        let storyBoard:UIStoryboard = UIStoryboard(name: "Addition", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "AddTemplate") as! AddTemplateController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension TemplateController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TemplateCell
        if (indexPath.row == dataToDisplay.count - 1) {
            cell.txtTitle.textAlignment = .center
            cell.txtTitle.textColor = UIColor(rgb: UInt(ColorScheme.linkColor))
        } else {
            cell.txtTitle.textAlignment = .left
            cell.txtTitle.textColor = UIColor(rgb: UInt(0x2F2F2F))
        }
        
        cell.txtTitle.text = dataToDisplay[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath.row == dataToDisplay.count - 1) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Addition", bundle: nil)
            let editControler = storyBoard.instantiateViewController(withIdentifier: "EditDelete") as! EditDeleteController
            self.navigationController?.pushViewController(editControler, animated: true)
        } else {
            // get all the viewControllers to start with
            if let controllers = self.navigationController?.viewControllers {
                // iterate through all the viewControllers
                for (index, controller) in controllers.enumerated() {
                    // when you find your current viewController
                    if controller == self {
                        // then you can get index - 1 in the viewControllers array to get the previous one
                        if let myUIViewController = controllers[index - 1] as? AddController {
                            myUIViewController.templateText = dataToDisplay[indexPath.row]
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
}
