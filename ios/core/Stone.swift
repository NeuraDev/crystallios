//
//  Stone.swift
//  ios
//
//  Created by Гладков Алексей on 22.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import UIKit

class Stone {
    static let stoneWidth = 60
    static let stoneHeight = 60
    static let maxStoneCount = 21
    fileprivate var container: UIView
    var stoneCount = 0
    
    init(container: UIView) {
        self.container = container
    }
    
    func generate(startX: Int, startY: Int, position: Int) -> UIImageView? {
        isHighlighted = false
        let imageView = UIImageView(frame: CGRect(x: startX + 15, y: startY + 15,
                                                  width: Stone.stoneWidth, height: Stone.stoneHeight))
//        imageView.backgroundColor = UIColor.white
//        imageView.backgroundColor = position == 0 ? UIColor.cyan : UIColor.blue
        imageView.image = UIImage(named: position == 0 ? "mini-crystall" : "mini-rock")
        container.addSubview(imageView)
            
        stoneCount = stoneCount + 1
        return imageView
    }
    
    func move(stone: UIView?, newX: CGFloat, newY: CGFloat) {
        if let strongStone = stone {
            strongStone.frame = CGRect(x: newX, y: newY, width:
                strongStone.frame.width, height: strongStone.frame.height)
        }
    }
    
    func disappear(stone: UIView, isDec: Bool) {
        stone.alpha = 0
        if (isDec) {
            stoneCount = stoneCount - 1
        }
    }
    
    var isHighlighted = false
    func highlight(stone: UIImageView?, container: UIView) {
        if (!isHighlighted) {
            isHighlighted = true
            if let strongStone = stone {
                let gif = UIImage(gifName: "crystall-expand-gif")
                let imageView = UIImageView(gifImage: gif, loopCount: 1)
                imageView.frame = CGRect(x: strongStone.frame.origin.x - 30,
                                         y: strongStone.frame.origin.y - 60,
                                         width: strongStone.frame.width + 60,
                                         height: strongStone.frame.height + 60)
                container.addSubview(imageView)
                strongStone.image = UIImage(named: "mini-crystall")
            }
        }
    }
    
    func shutdown(stone: UIImageView?) {
        if let strongStone = stone {
            strongStone.image = UIImage(named: "mini-rock")
        }
    }
}
