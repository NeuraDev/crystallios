//
//  Finger.swift
//  ios
//
//  Created by Гладков Алексей on 22.01.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation
import UIKit

protocol MapInterface {
    func crossMiddle(state: Int)
    func onRelease(state: Int)
    func onHold()
}

class Finger {
    fileprivate var callback: MapInterface? = nil
    fileprivate var container: UIView
    
    init(container: UIView) {
        self.container = container
        initInteraction()
    }
    
    func attachCallback(callback: MapInterface) {
        self.callback = callback
    }
    
    func detachCallback() {
        self.callback = nil
    }
    
    @IBAction private func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        print("handler tap called")
        if (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
            let translation = gestureRecognizer.translation(in: container)
            print("move x \(translation.x), y \(translation.y)")
        }
    }
    
    private func initInteraction() {
        print("gesture reco inited")
        let gestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handlePan(gestureRecognizer:)))
        container.addGestureRecognizer(gestureRecognizer)
    }
}
