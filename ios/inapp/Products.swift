//
//  Products.swift
//  ios
//
//  Created by Гладков Алексей on 01.03.18.
//  Copyright © 2018 Applihelp. All rights reserved.
//

import Foundation

public struct Products {
    public static let coffee = "crystall.coffee"
    public static let fresh = "crystall.fresh"
    public static let lunch = "crystall.lunch"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [Products.coffee, Products.fresh, Products.lunch]
    public static let store = IAPHelper(productIds: Products.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
